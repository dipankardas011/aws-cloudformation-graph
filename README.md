# AWS Resource Dependency Graph Generator

## Why?
As AWS doesn't provide a way to get a dependency grpah for what all resources connected
this tool will help you generated the a diagram for all the dependencies between resources

## Resources currently supported

### EC2
![](./cover_ec2.png)

### CloudFormation
![](./cover_cloudformation.png)
