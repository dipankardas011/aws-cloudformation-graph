package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/opts"
)

type DataStack struct {
	LogicalResID []string
	ResourceType string
}

type DependencyGraph struct {
	Resources map[string][]DataStack
}

func ec2Graph(sess *session.Session) {
	clientEc2 := ec2.New(sess, aws.NewConfig())

	graph := DependencyGraph{
		Resources: make(map[string][]DataStack),
	}

	result, err := clientEc2.DescribeInstances(&ec2.DescribeInstancesInput{})
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	for _, reservation := range result.Reservations {
		for _, instance := range reservation.Instances {
			// vpc
			if instance.VpcId != nil {
				graph.Resources[*instance.InstanceId] = append(graph.Resources[*instance.InstanceId], DataStack{
					ResourceType: "vpc",
					LogicalResID: []string{*instance.VpcId},
				})
			}

			// subnet
			if instance.SubnetId != nil {
				graph.Resources[*instance.InstanceId] = append(graph.Resources[*instance.InstanceId], DataStack{
					ResourceType: "subnet",
					LogicalResID: []string{*instance.SubnetId},
				})
			}

			// Security groups
			if instance.SecurityGroups != nil {
				graph.Resources[*instance.InstanceId] = append(graph.Resources[*instance.InstanceId], DataStack{
					ResourceType: "security-group",
				})
				for _, sgs := range instance.SecurityGroups {
					graph.Resources[*instance.InstanceId][len(graph.Resources[*instance.InstanceId])-1].LogicalResID = append(graph.Resources[*instance.InstanceId][len(graph.Resources[*instance.InstanceId])-1].LogicalResID, *sgs.GroupId)
				}
			}

			// ssh key
			if instance.KeyName != nil {
				graph.Resources[*instance.InstanceId] = append(graph.Resources[*instance.InstanceId], DataStack{
					ResourceType: "keypair",
					LogicalResID: []string{*instance.KeyName},
				})
			}

			// for Elastic block storage
			if instance.BlockDeviceMappings != nil {
				blockStore := instance.BlockDeviceMappings
				for _, block := range blockStore {
					if block.Ebs != nil {
						graph.Resources[*instance.InstanceId] = append(graph.Resources[*instance.InstanceId], DataStack{
							ResourceType: "ebs",
							LogicalResID: []string{*block.Ebs.VolumeId},
						})
					}
				}
			}

			if instance.NetworkInterfaces != nil {
				// for the network interface connected
				netInterfaces := instance.NetworkInterfaces
				for _, netInterface := range netInterfaces {
					graph.Resources[*instance.InstanceId] = append(graph.Resources[*instance.InstanceId], DataStack{
						ResourceType: "network-interface",
						LogicalResID: []string{*netInterface.NetworkInterfaceId},
					})
				}

				// for getting any elastic IP associated
				associations := instance.NetworkInterfaces
				for _, associated := range associations {
					if associated.Association != nil {
						graph.Resources[*instance.InstanceId] = append(graph.Resources[*instance.InstanceId], DataStack{
							ResourceType: "elastic IP",
							LogicalResID: []string{*associated.Association.PublicDnsName},
						})
					}
				}
			}
		}
	}

	fmt.Println(graph)
	graphE := charts.NewGraph()
	graphE.SetGlobalOptions(charts.WithTitleOpts(opts.Title{
		Title: "AWS EC2 all the instances",
		Subtitle: `Legends
- Yellow denotes the ec2 instance
- Green denotes the Resource_name<>Resource_Type`,
		SubtitleStyle: &opts.TextStyle{Color: "black"},
	}))
	nodes, links := transformDependencyGraphToGraphData(graph)

	graphE.AddSeries("Dependency Graph", nodes, links).SetSeriesOptions(
		charts.WithLabelOpts(opts.Label{FontStyle: "italic"}),
	)

	f, _ := os.Create("graph_ec2.html")
	graphE.Render(f)
}

func cloudFormationGraph(sess *session.Session) {

	graph := DependencyGraph{
		Resources: make(map[string][]DataStack),
	}
	svc := cloudformation.New(sess, aws.NewConfig())

	stack, err := svc.ListStacks(&cloudformation.ListStacksInput{})
	if err != nil {
		fmt.Println("all stack failed")
		return
	}

	for _, stackSet := range stack.StackSummaries {
		tempSumm, err := svc.GetTemplateSummary(&cloudformation.GetTemplateSummaryInput{
			StackName: stackSet.StackName,
		})
		if err != nil {
			fmt.Printf("[WARN] GET Stack {%v} Reason: %v\n", stackSet.StackId, err)
			continue
		}
		for _, resources := range tempSumm.ResourceIdentifierSummaries {
			strSlice := make([]string, 0, len(resources.LogicalResourceIds))
			for _, ptr := range resources.LogicalResourceIds {
				strSlice = append(strSlice, *ptr)
			}
			if _, present := graph.Resources[*stackSet.StackName]; !present {
				graph.Resources[*stackSet.StackName] = []DataStack{DataStack{ResourceType: *resources.ResourceType, LogicalResID: strSlice}}
			} else {
				graph.Resources[*stackSet.StackName] = append(graph.Resources[*stackSet.StackName], DataStack{ResourceType: *resources.ResourceType, LogicalResID: strSlice})
			}
		}
	}

	fmt.Println(graph)
	graphE := charts.NewGraph()
	graphE.SetGlobalOptions(charts.WithTitleOpts(opts.Title{
		Title: "AWS cloudformation all the stack",
		Subtitle: `Legends
- Yellow denotes the Stack
- Green denotes the Resource_name<>Resource_Type`,
		SubtitleStyle: &opts.TextStyle{Color: "black"},
	}))
	nodes, links := transformDependencyGraphToGraphData(graph)

	graphE.AddSeries("Dependency Graph", nodes, links).SetSeriesOptions(
		// charts.WithGraphChartOpts(opts.GraphChart{Layout: "circular", EdgeSymbol: "circle", EdgeSymbolSize: 5}), // comment to get free float one
		charts.WithLabelOpts(opts.Label{FontStyle: "italic"}),
	)

	f, _ := os.Create("graph_cloudformation.html")
	graphE.Render(f)
}

func main() {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	cloudFormationGraph(sess)
	ec2Graph(sess)
}

func transformDependencyGraphToGraphData(graph DependencyGraph) ([]opts.GraphNode, []opts.GraphLink) {
	nodes := make([]opts.GraphNode, 0)
	links := make([]opts.GraphLink, 0)

	nodePresent := make(map[string]bool, 0)

	for resourceName, dependResources := range graph.Resources {
		if _, present := nodePresent[resourceName]; !present {
			nodes = append(nodes, opts.GraphNode{Name: resourceName, Symbol: "diamond", ItemStyle: &opts.ItemStyle{Color: "#fbbc7f"}})
			nodePresent[resourceName] = true
		}
		for _, resource := range dependResources {
			for _, logicalResID := range resource.LogicalResID {
				if _, present := nodePresent[logicalResID+"<>"+resource.ResourceType]; !present {
					nodes = append(nodes, opts.GraphNode{Name: logicalResID + "<>" + resource.ResourceType, Symbol: "rect", ItemStyle: &opts.ItemStyle{Color: "#a7c080"}})
					nodePresent[logicalResID+"<>"+resource.ResourceType] = true
				}
				links = append(links, opts.GraphLink{Source: resourceName, Target: logicalResID + "<>" + resource.ResourceType})
			}
		}
	}

	return nodes, links
}
