module abcd

go 1.20

require (
	github.com/aws/aws-sdk-go v1.44.280
	github.com/go-echarts/go-echarts/v2 v2.2.6
)

require github.com/jmespath/go-jmespath v0.4.0 // indirect
